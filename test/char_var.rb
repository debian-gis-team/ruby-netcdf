require 'test/unit'
require 'numru/netcdf'
include NumRu
class TestCharVar < Test::Unit::TestCase
  def setup
    @s = 'tmp.nc'
    f = NetCDF.create(@s)
    d = f.def_dim('x',5)
    v = f.def_var('x','char',[d])
    tv = f.def_var('text','char',[d])
    f.enddef
    v.put( NArray.byte(5).indgen! )
    tv.put( NArray.to_na("hello","byte",5) )
    tv.put( NArray.to_na("LO","byte",2), 'start'=>[3] )
    tv.put( NArray.to_na("H","byte",1), 'index'=>[0] )
    f.close
  end
  def teardown
    if File.exist?(@s)
      begin
        File.delete(@s)
      rescue
        p $!
      end
    end
  end

  def test_char_var
  f = NetCDF.open(@s)
  v = f.var('x')
  assert_equal v.get, NArray.byte(5).indgen!
  tv = f.var('text')
  assert_equal tv.get.to_s, "HelLO"
  end

end
