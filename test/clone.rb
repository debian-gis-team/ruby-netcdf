require 'test/unit'
require 'numru/netcdf'
include NumRu

class TestClone < Test::Unit::TestCase
  def setup
    @s = 'tmp.nc'
    @f = NetCDF.create(@s)
    @f.redef
    @d = @f.def_dim('x',3)
    @v = @f.def_var('x','sfloat',[@d])
    @a = @f.put_att('long_name','xx')
    @f.enddef
    @v.put([1,2,3])
    @f.freeze
  end

  def teardown
    @f.close
    if File.exist?(@s)
      begin
        File.delete(@s)
      rescue
        p $!
      end
    end
  end

  def test_clone_netcdf_path
    f = @f.clone
    assert_equal @f.path, f.path
    assert(@f.frozen? == f.frozen?)
    d = @d.clone
    assert_equal @d, d
    v = @v.clone
    assert_equal @v, v
    a = @a.clone
    assert_equal @a, a
  end

  def test_clone_netcdf_dup
    f = @f.dup
    assert(@f.frozen? != f.frozen?)
    # d = @d.dup
    # v = @v.dup
    # a = @a.dup
  end

end
