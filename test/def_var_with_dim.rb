require 'test/unit'
require 'numru/netcdf'
include NumRu
class TestDefVarWithDim < Test::Unit::TestCase
  def setup
    @fname = "tmp.nc"
    @file = NetCDF.create(@fname)
    @var1 = @file.def_var_with_dim("var1","sfloat",[6],["x"])
    @var2 = @file.def_var_with_dim("var2","sfloat",[6,3],["x","y"])
    @var3 = @file.def_var_with_dim("var3","sfloat",[3],["y"])
    @var3 = @file.def_var_with_dim("var4","sfloat",[0],["t"])
    @att = @var1.put_att("long_name","test")
    @file.close
  end
  def teardown
    if File.exist?(@fname)
      begin
        File.delete(@fname)
      rescue
        p $!
      end
    end
  end

  def test_def_var_with_dim
    assert_not_nil @var1
    assert_not_nil @var2
    assert_not_nil @var3
    assert_not_nil @file
    assert_not_nil @att
  end
end
