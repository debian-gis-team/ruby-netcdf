require 'test/unit'
require 'numru/netcdf'
include NumRu

# What checked?

class TestCreate < Test::Unit::TestCase

  def test_create_netcdf_file
    @filename = "test.nc"
    file=NetCDF.create(@filename,false,false)
    assert dimx=file.def_dim("x",15)
    assert dimy=file.def_dim("y",10)
    assert dimz=file.def_dim("z",10)
    batt = file.put_att("type_byte",5,"byte")
    file.put_att("type_short",[222,333,444],"sint")
    file.put_att("type_int",[2222,3333,4444])
    file.put_att("type_float",[2.22,3.33,4.44],"sfloat")
    file.put_att("type_double",[2.222,3.333,4.444])
    string = file.put_attraw("string","netCDF for Ruby","string")
    batt.put(6,"byte")

    assert sint_var=file.def_var("test_sint","sint",["x"])
    assert byte_var=file.def_var("test_byte","byte",["y"])
    assert byte_var2=file.def_var("test_byte2","byte",[dimy,dimz])
    assert int_var=file.def_var("test_int","int",["y"])
    assert sfloat_var=file.def_var("test_sfloat","sfloat",["z"])
    assert float_var=file.def_var("test_float","float",["y"])

    assert a=NArray.sint(10).indgen
    assert b=NArray.sint(10).fill(7)
    assert c=NArray.byte(10).indgen
    assert d=NArray.int(10).indgen
    assert e=NArray.sfloat(10).fill(1.111)
    assert f=NArray.float(10).fill(5.5555555)
    file.enddef
    file2 = file
    assert_equal file2, file

    assert_nil byte_var.put_var_byte(c)
    assert_nil int_var.put_var_int(d)
    assert_nil sfloat_var.put_var_sfloat(e)
    assert_nil float_var.put_var_float(f)

    file.redef
    byte_vara=file.def_var("test_byte_vara","byte",[dimy,dimz]);
    sint_vara=file.def_var("test_sint_vara","sint",["y","z"]);
    int_vara=file.def_var("test_int_vara","int",["y","z"]);
    sfloat_vara=file.def_var("test_sfloat_vara","sfloat",["y","z"]);
    float_vara=file.def_var("test_float_vara","float",["y","z"]);
    file.enddef

    byte_vara2 = byte_vara
    assert_equal byte_vara2, byte_vara

    assert g=NArray.byte(10,10).indgen
    assert h=NArray.byte(2,3).indgen
    assert gh=NArray.byte(1,1).fill(33)
    assert k=NArray.sint(10,10).indgen
    assert l=NArray.sint(2,3).indgen
    assert kl=NArray.sint(1,1).fill(44)
    assert m=NArray.int(10,10).indgen
    assert n=NArray.int(2,3).indgen
    assert mn=NArray.int(1,1).fill(55)
    assert o=NArray.sfloat(10,10).fill(1.234567)
    assert p=NArray.sfloat(2,3).fill(2.345678)
    assert op=NArray.int(1,1).fill(3.4)
    assert q=NArray.float(10,10).fill(1.234)
    assert r=NArray.float(2,3).fill(2.345)
    assert qr=NArray.float(1,1).fill(4.5)
    assert s=NArray.float(2,2).fill(10.0)

    assert_nil byte_vara.put(g)
    assert_nil byte_vara.put(h,{"start"=>[3,5]})
    assert_nil byte_vara.put(g,{"start"=>[0,0],"end"=>[9,9]})
    assert_nil byte_vara.put(h,{"start"=>[-8,2]})
    assert_nil byte_vara.put(h,{"start"=>[0,0],"stride"=>[2,3]})
    assert_nil byte_vara.put(h,{'start'=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    assert_nil byte_vara.put(gh,{"index"=>[4,7]})

    sint_vara.put(k)
    sint_vara.put(l,{"start"=>[3,5]})
    sint_vara.put(k,{"start"=>[0,0],"end"=>[9,9]})
    sint_vara.put(l,{"start"=>[-8,2]})
    sint_vara.put(l,{"start"=>[0,0],"stride"=>[2,3]})
    sint_vara.put(l,{"start"=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    sint_vara.put(kl,{"index"=>[4,7]})

    int_vara.put(m)
    int_vara.put(n,{"start"=>[3,5]})
    int_vara.put(m,{"start"=>[0,0],"end"=>[9,9]})
    int_vara.put(n,{"start"=>[-8,2]})
    int_vara.put(n,{"start"=>[0,0],"stride"=>[2,3]})
    int_vara.put(n,{"start"=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    int_vara.put(mn,{"index"=>[4,7]})

    sfloat_vara.put(o)
    sfloat_vara.put(p,{"start"=>[3,5]})
    sfloat_vara.put(o,{"start"=>[0,0],"end"=>[9,9]})
    sfloat_vara.put(p,{"start"=>[-8,2]})
    sfloat_vara.put(p,{"start"=>[0,0],"stride"=>[2,3]})
    sfloat_vara.put(p,{"start"=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    sfloat_vara.put(op,{"index"=>[4,7]})

    float_vara.put(q)
    float_vara.put(r,{"start"=>[3,5]})
    float_vara.put(q,{"start"=>[0,0],"end"=>[9,9]})
    float_vara.put(r,{"start"=>[-8,2]})
    float_vara.put(r,{"start"=>[0,0],"stride"=>[2,3]})
    float_vara.put(r,{"start"=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    float_vara.put(qr,{"index"=>[4,7]})

    float_vara.dim_names

    assert na_aaa=byte_vara.get({"start"=>[3,5]})
    assert na_aab=byte_vara.get({"start"=>[0,0],"end"=>[9,9]})
    assert na_aac=byte_vara.get({"start"=>[-8,2]})
    assert na_aad=byte_vara.get({"start"=>[0,0],"stride"=>[2,3]})
    assert na_aae=byte_vara.get({'start'=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    assert na_aaf=byte_vara.get({"index"=>[4,7]})
    assert na_aag=byte_vara.get

    assert na_bba=sint_vara.get({"start"=>[3,5]})
    assert na_bbb=sint_vara.get({"start"=>[0,0],"end"=>[9,9]})
    assert na_bbc=sint_vara.get({"start"=>[-8,2]})
    assert na_bbd=sint_vara.get({"start"=>[0,0],"stride"=>[2,3]})
    assert na_bbf=sint_vara.get({"start"=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    assert na_bbg=sint_vara.get({"index"=>[4,7]})
    assert na_bbh=sint_vara.get

    assert na_cca=int_vara.get({"start"=>[3,5]})
    assert na_ccb=int_vara.get({"start"=>[0,0],"end"=>[9,9]})
    assert na_ccc=int_vara.get({"start"=>[-8,2]})
    assert na_ccd=int_vara.get({"start"=>[0,0],"stride"=>[2,3]})
    assert na_cce=int_vara.get({"start"=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    assert na_ccf=int_vara.get({"index"=>[4,7]})
    assert na_ccg=int_vara.get

    assert na_dda=sfloat_vara.get({"start"=>[3,5]})
    assert na_ddb=sfloat_vara.get({"start"=>[0,0],"end"=>[9,9]})
    assert na_ddc=sfloat_vara.get({"start"=>[-8,2]})
    assert na_ddd=sfloat_vara.get({"start"=>[0,0],"stride"=>[2,3]})
    assert na_dde=sfloat_vara.get({"start"=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    assert na_ddf=sfloat_vara.get({"index"=>[4,7]})
    assert na_ddg=sfloat_vara.get

    assert na_eea=float_vara.get({"start"=>[3,5]})
    assert na_eeb=float_vara.get({"start"=>[0,0],"end"=>[9,9]})
    assert na_eec=float_vara.get({"start"=>[-8,2]})
    assert na_eed=float_vara.get({"start"=>[0,0],"stride"=>[2,3]})
    assert na_eee=float_vara.get({"start"=>[1,1],"end"=>[3,7],"stride"=>[2,3]})
    assert na_eef=float_vara.get({"index"=>[4,7]})
    assert na_eeg=float_vara.get

    file.redef

    copy_byte = string.copy(byte_vara)
    copy_byte.put([0,20],"byte")
    copy_byte.name="new_name"

    copy_byte2 = copy_byte
    assert_equal copy_byte2, copy_byte

    copy_sint = string.copy(sint_vara)
    copy_sint.put("%%%%%")
    copy_int = string.copy(int_vara)
    copy_int.put([0,60],"int")
    copy_sfloat = string.copy(sfloat_vara)
    copy_sfloat.put([0.01,5.5],"sfloat")
    copy_float = string.copy(float_vara)
    copy_float.put([0.0001,5.5555],"float")
    file.enddef
    assert nm = copy_byte.name
    assert att0 = string.get
    assert att1 = copy_byte.get
    assert att2 = copy_sint.get
    assert att3 = copy_int.get
    assert att4 = copy_sfloat.get
    assert att5 = copy_float.get
    file.fill(true)
    file.fill(false)

    float_vara.dim_names
    float_vara.att_names

    file.close
  end

  def teardown
    if File.exist?(@filename)
      begin
        File.delete(@filename)
      rescue
        p $!
      end
    end
  end

end
