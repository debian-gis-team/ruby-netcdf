require 'test/unit'
require 'numru/netcdf'
include NumRu
class CreateTMP < Test::Unit::TestCase
  def setup
    @file = NetCDF.create_tmp
    @file.def_dim('x',5)
    @file.put_att("history", __FILE__ )
  end
  def teardown
    @file.close
    GC.start
  end
  def test_create_tmp
    assert_equal @file.att("history").get, __FILE__
    # print "environment variable TEMP ="+(ENV['TEMP'] || '')+"\n"
    # file2 = @file
  end
end
