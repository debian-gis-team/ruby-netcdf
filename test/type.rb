require 'test/unit'
require 'numru/netcdf'
include NumRu
class TestType < Test::Unit::TestCase
  def test_type
    @s = 'tmp.nc'
    f = NetCDF.create(@s)
    d = f.def_dim('x',2)
    v = f.def_var('x','sfloat',[d])
    assert_equal v.vartype,
                 v.ntype
    v = f.def_var('x1','sint',[d])
    assert_equal v.vartype,
                 v.ntype
    v = f.def_var('x2','byte',[d])
    assert_equal v.vartype,
                 v.ntype
    f.close
  end
  def teardown
    if File.exist?(@s)
      begin
        File.delete(@s)
      rescue
        p $!
      end
    end
  end
end
